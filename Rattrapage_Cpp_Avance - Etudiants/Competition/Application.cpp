#include <iostream>
#include <Windows.h>
#include "Application.h"
#include "Concurrent.h"

using namespace std;

/// <summary>
/// Retourne un nombre entier g�n�r� au hasard, n,
/// avec deb <= n < fin
/// </summary>
/// <param name="deb"> la valeur minimale de n</param>
/// <param name="fin"> la valeur � laquelle n doit �tre < </param>
/// <returns> n avec deb <= n < fin </returns>
int hasard(int deb, int fin)
{
	int val;
	val = (rand() % fin) + deb;
	return val;
}


/// <summary>
/// Initialiser la comp�tition.
/// Consiste � saisir le nombre de dossards n�cessaires (= nombre de concurrents)
/// puis � les cr�er dans le conteneur dossardsPourAffectation.
/// Entre 2 et 100 dossards.
/// </summary>
void Application::InitialiserCompetition()
{
	int nombreDossards;
	do {
		std::cout << "Veuillez entrer le nombre de dossards n�cessaires (entre 2 et 100) : ";
		std::cin >> nombreDossards;
	} while (nombreDossards < 2 || nombreDossards > 100);

	for (int i = 1; i <= nombreDossards; ++i) {
		dossardPourAffection.push_back(i);
	}

	std::cout << "La comp�tition a �t� initialis�e avec " << nombreDossards << " dossards." << std::endl;
}

/// <summary>
/// Inscrire un concurent.
/// Apr�s v�rification qu'il reste un dossard disponible, il faut:
/// - saisir le nom du concurrent
/// - retirer 1 dossard au hasard dans le conteneur dossardsPourAffectation
/// - cr�er le concurrent avec son nom et le dossard
/// - ajouter le concurrent au conteneur concurentsInscrits
/// Pas d'inscription possible s'il ne reste plus de dossard disponible.
/// </summary>
void Application::InscrireUnConcurrent()
{
	if (!dossardPourAffection.empty()) {
		int dossard = dossardPourAffection.front();
		dossardPourAffection.pop_front();
		
		std::string nom;
		std::cout << "Entrez un nom : ";
		std::cin >> nom;

		concurrentsInscrits.push_back(nom);
		concurrentsInscrits[dossard] = nom;

		std::cout << "Le nom \"" << nom << "\" a �t� inscrit avec le dossard : " << dossard << std::endl;

	}
	else {
		std::cout << "Il n'y a plus de dossards disponibles !" << std::endl;
	}

}

/// <summary>
/// Affiche la liste des concurrents inscrits, par ordre alphab�tique des noms
/// </summary>
void Application::AfficherParNom()
{
	std::cout << "Tri�e par nom :" << std::endl;
	for (const auto& entry : concurrentsInscrits) {
		std::cout << "Nom: " << entry.second << ", Dossard: " << entry.first << std::endl;
	}
}

/// <summary>
/// Affiche la liste des concurrents inscrits, par ordre croissant des dossards
/// </summary>
void Application::AfficherParDossard()
{
	std::cout << "Ordre croissant des dossards :" << std::endl;
	for (const auto& entry : concurrentsInscrits) {
		std::cout << "Dossard: " << entry.first << ", Nom: " << entry.second << std::endl;
	}
}

/// <summary>
/// Permet de noter (scorer) tous les concurrents.
/// - extrait et supprime le premier concurrent du conteneur concurentsInscrits
/// - lui affecte un score entre 0 et 10 inclus
/// - ins�re le concurrent not� dans le conteneur resultats, les mieux not�s rang�s en premier
/// Le score doit servir de cl� pour retrouver tous les concurrents ayant un certain score.
/// Attention!! On peut avoir plusieurs concurrents avec le m�me score.
/// </summary>
void Application::NoterConcurrents()
{
	//@TODO � compl�ter.
	std::cout << "Notation des concurrents..." << std::endl;

	while (!concurrentsInscrits.empty()) {
		int dossard = concurrentsInscrits.begin()->first;
		std::string nom = concurrentsInscrits.begin()->second;
		concurrentsInscrits.erase(concurrentsInscrits.begin());

		int score;
		std::cout << "Entrez le score pour le concurrent \"" << nom << "\" (entre 0 et 10) : ";
		std::cin >> score;

		resultats[score] = nom;

		std::cout << "Le concurrent \"" << nom << "\" a �t� not� avec un score de " << score << std::endl;
	}
}

/// <summary>
/// Affiche le score, le dossard et le nom des concurrents not�s.
/// </summary>
void Application::AfficherResultats()
{
	//@TODO � compl�ter.
	std::cout << "R�sultats de la comp�tition :" << std::endl;
	for (const auto& entry : resultats) {
		int score = entry.first;
		std::string nom = entry.second;
		std::cout << "Score: " << score << ", Nom: " << nom << std::endl;
	}

}

/// <summary>
/// Boucle d'ex�cution du programme.
/// </summary>
void Application::Run()
{
	bool quit = false;
	int choix = 0;

	do
	{
		//  Affiche le menu et lit le choix de l'utilisateur
		menu.Affiche();
		choix = menu.SaisirEntierEntre(1, 6);

		switch(choix)	//  R�alise l'action choisie par l'utilisateur
		{
		case 1:
			InscrireUnConcurrent();
			break;
		case 2:
			AfficherParNom();
			break;
		case 3:
			AfficherParDossard();
			break;
		case 4:
			NoterConcurrents();
			break;
		case 5:
			AfficherResultats();
			break;
		case 6:
			quit = true;
			break;
		}
	} while (!quit);
}
